package es.ngjava.bootswagger.web.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

	@GetMapping("/")
	public ResponseEntity<Object> home(){
		return new ResponseEntity<Object>("Home", HttpStatus.OK);
	}
	
	@GetMapping("/hello")
	public ResponseEntity<Object> hello(){
		return new ResponseEntity<Object>("Hello World", HttpStatus.OK);
	}
	
}
